<!--
Copyright (c) 2021 The Yaook Authors.

This file is part of Yaook.
See https://yaook.cloud for further info.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
# Gnocchi

The docker image for the [Gnocchi](https://github.com/gnocchixyz/gnocchi) service.

## Differences to other Yaook Images

This image contains Gnocchi which is not a project of the OpenStack project.
The sources are located at github.com.

The image contains a build of Python bindings for Ceph. The ceph version used is based on the
version of the rados-package in the OS of the base image.

## Build arguments

The `Dockerfile` used for the image build has two build arguments:

- `gnocchi_release` which indicates the version of gnocchi to use. It defaults to `4.4.2.
- `openstack_release` which indicates for which release of OpenStack the image
  should be build. It is used to retrieve the python upper constraints via the
  requirements repository. It defaults to `queens"

## License

[Apache 2](LICENSE.txt)
